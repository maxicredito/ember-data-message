# Ember-data-i18n

This README outlines the details of collaborating on this Ember addon.

## Installation

* `git clone` this repository
* `npm install`
* `bower install`

## Running

* `ember server`
* Visit your app at http://localhost:4200.

## Running Tests

* `ember test`
* `ember test --server`

## Building

* `ember build`

For more information on using ember-cli, visit [http://www.ember-cli.com/](http://www.ember-cli.com/).

##To Use
Add in your package.json:

```javascript
"ember-data-message":"git+ssh://git@bitbucket.org/maxicredito/ember-data-message#master"
```

And must be ember-flash-messages too, for example:

```javascript
"ember-flash-messages": "^0.5.2",
```

And bootstrap too, when do you want more beautiful messages:

```javascript
"bootstrap": "^3.3.4"
```

**Needs the user ssh public key is allowed in bitbucket to download de dependency.**

```hbs
{{! in app/templates/application.hbs}}
{{message-notify message='teste para message:notify' type='danger'}}

{{ds-message errors=errors}}
```
And controller file:
```javascript
#app/controllers/application.js
import Ember from 'ember';

export default Ember.Controller.extend({
  errors: function() {
    return {
      "error": [
        {"message": "error1"},
        {"message": "error2"}
      ]
    };
  }.property()
});
```

For more information about type messages visit https://github.com/sir-dunxalot/ember-flash-messages.
