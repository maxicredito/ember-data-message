import Ember from "ember";
import { test } from 'ember-qunit';
import startApp from '../helpers/start-app';

var App;

module("Acceptance: message-notify", {
  setup: function(){
    App = startApp();
  },
  teardown: function(){
    App.reset();
  }
});

test('Show Message', function() {
  expect(1);
  visit('/');
  andThen(function() {
    var text = find('div.alert-danger:contains(teste para message\:notify)');
    equal(1, text.length);
  });
});
