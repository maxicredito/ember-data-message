import Ember from "ember";
import { test } from 'ember-qunit';
import startApp from '../helpers/start-app';

var App;

module("Acceptance: ds-message", {
  setup: function(){
    App = startApp();
  },
  teardown: function(){
    App.reset();
  }
});

test('Show Message', function() {
  expect(2);
  visit('/');
  andThen(function() {
    var text = find('div.alert-danger:contains(error1)');
    equal(1, text.length);
    text = find('div.alert-danger:contains(error2)');
    equal(1, text.length);
  });
});
