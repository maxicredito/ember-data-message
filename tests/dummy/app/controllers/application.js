import Ember from 'ember';

export default Ember.Controller.extend({
  errors: function() {
    return {
      "error": [
        {"message": "error1"},
        {"message": "error2"}
      ]
    };
  }.property()
});
